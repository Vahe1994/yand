import telebot
import datetime
import random
import time
import logging

import pandas as pd
from sklearn.ensemble import VotingClassifier
from sklearn.neural_network import MLPClassifier
import numpy as np
import sklearn as sk
from sklearn import tree
from sklearn.model_selection import cross_val_score
import sklearn.model_selection as skm
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import GradientBoostingClassifier
import tweepy
from tweepy import OAuthHandler
import pickle
from sklearn.externals import joblib
import re

token = '424978722:AAGivkDQ0Lo7CVrFJqdoV60kU1iGgCTuNyg'
bot = telebot.TeleBot(token)
clf = joblib.load('ens_clf.pkl')
Wheaherdict = []
logger = telebot.logger
telebot.logger.setLevel(logging.INFO)


def remove_cruft(s):
    p = re.compile("(?:https?://)?(?:www\.|mobile\.)?twitter\.com/(?:#!/)?@?([^/\?]*)|")
    return re.match(p, s).groups()[0]


def NewFeatures(author_url):
    if (author_url.text[0] == '['):
        author_url = pd.read_json(author_url.text)
        author_url = author_url.url
    else:
        author_url = str(author_url.text).replace('\n', ' ')
        author_url = str(author_url).split(" ")
    users = [remove_cruft(s) for s in author_url]
    CONSUMER_KEY = 'aOt2IH2XvHswwRCPczoHtGXDU'
    CONSUMER_SECRET = 'lHr5ej8usxk4GzpF9HtyGPbwhhBbkPuJURKOdpvaBgmF6JSPSS'
    ACCESS_KEY = '3390972021-qUTZk0E80oycfeqhTAeRuKVrMYWAWuPbHuHFWBK'
    ACCESS_SECRET = 'LGhbhbvOEfspzJeRyYSUtNjKpGn76650RwnVahEuxMeLX'
    auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    api = tweepy.API(auth)
    auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)

    # search
    api = tweepy.API(auth)
    today = datetime.date.today()
    today = datetime.datetime.combine(today, datetime.datetime.min.time())
    test = api.lookup_users(screen_names=users[:])
    more_data = []
    for user in test:
        more_data.append({'user_name': user.screen_name,
                          'Name': user.name,
                          'followers_count': user.followers_count,
                          'statuses_count': user.statuses_count,
                          'friends_count': user.friends_count,
                          'listed_count': user.listed_count,
                          'Days_pass': (today - user.created_at).days,
                          'favourites_count': user.favourites_count,
                          'follow_request_sent': user.follow_request_sent,
                          'has_extended_profile': user.has_extended_profile,
                          'User language': user.lang,
                          'User location': user.location,
                          'Notifications': user.notifications,
                          'backgroung image url': user.profile_background_image_url_https,
                          'Image url': user.profile_image_url,
                          'Verified': user.verified,
                          'Protected': user.protected,
                          'author_url': 'http://twitter.com/' + user.screen_name})
    df = pd.DataFrame(data=more_data)
    return df


@bot.message_handler(commands=['start'])
def handle_start_help(message):
    bot.send_message(message.chat.id,
                     "Hello! Welcome to spamer hunt bot. Let's hunt Twitter spamer! For help type /help")


@bot.message_handler(commands=['help'])
def handle_start_help(message):
    bot.send_message(message.chat.id,
                     'To classificate users you can use json commands like: \n [\n{"url": "http://twitter.com/uertuyer"},\n{"url": "http://twitter.com/chxkvhxs"},\n{"url": "http://twitter.com/hsdkfhds"}\n] \n or you can simpy just type url of twitter accounts with split of spaces and new lines like: \n http://twitter.com/hsdkfhds \n http://twitter.com/uertuyer http://twitter.com/chxkvhxs')
    bot.send_message(message.chat.id,
                     'For any bug report you can send report to vahe527887@yandex.ru .\n Thank you for support.Good Hunt and have fun! ;)')


@bot.message_handler(content_types=["text"])
def just_do_t(message):
    data = NewFeatures(message)
    x_list = ['statuses_count', 'listed_count', 'friends_count', 'favourites_count', 'followers_count', 'Days_pass',
              'Verified']
    x_all = data[x_list]
    y_all = clf.predict_proba(x_all)
    result = ''
    for i in range(len(y_all)):
        if (i == 0):
            result = '[\n'
        score = np.max(y_all[i])
        oclass = np.argmax(y_all[i])
        if (oclass == 2):
            cl = "spam"
        else:
            if (oclass == 0):
                cl = "human"
            else:
                cl = "other"
        if (i != len(y_all) - 1):
            result += '{\n"url":"' + data['author_url'][i] + '",\n"class":"' + cl + '",\n"score":' + str(
                score) + '\n},\n'
        else:
            result += '{\n"url":"' + data['author_url'][i] + '",\n"class":"' + cl + '",\n"score":' + str(
                score) + '\n}\n]'
    bot.send_message(message.chat.id, result)


if __name__ == '__main__':
    while True:
        try:
            bot.polling(none_stop=True, timeout=60)
        except Exception as e:
            logger.error(e)
            time.sleep(15)
